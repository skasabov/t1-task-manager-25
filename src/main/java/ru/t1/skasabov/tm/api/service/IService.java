package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.api.repository.IRepository;
import ru.t1.skasabov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
